﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class MoonGenerator
    {
        public MoonGenerator() { }

        public Moon Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            Moon moon = new Moon();

            moon.Diameter = random.NextDouble(0.4);
            moon.Mass = random.NextDouble() * moon.Diameter;

            return moon;
        }
    }
}
