﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class TerrainGenerator
    {
        public TerrainGenerator() { }

        public Terrain Generate(PlanetType planetType)
        {
            RandomSingleton random = RandomSingleton.Instance();
            Terrain terrain = new Terrain();

            switch (planetType)
            {
                case PlanetType.Terrestrial:
                    double chanceOfFeatures = random.NextDouble();
                    if (chanceOfFeatures < 0.5)
                        terrain.AddFeature(TerrainFeature.OtherLand, 100.0);
                    else
                    {
                        double OceanChance, DesertChance, ForestChance, MountainChance;
                        OceanChance = random.NextDouble();
                        if (OceanChance < 0.3)
                            terrain.AddFeature(TerrainFeature.Ocean, random.NextDouble(10.0, 40.0));
                        DesertChance = random.NextDouble();
                        if (DesertChance < 0.6)
                            terrain.AddFeature(TerrainFeature.Desert, random.NextDouble(10.0, 20.0));
                        ForestChance = random.NextDouble();
                        if (ForestChance < 0.5)
                            terrain.AddFeature(TerrainFeature.Forest, random.NextDouble(20.0, 30.0));
                        MountainChance = random.NextDouble();
                        if (MountainChance < 0.8)
                            terrain.AddFeature(TerrainFeature.Moutain, random.NextDouble(10.0));

                        terrain.AddFeature(TerrainFeature.OtherLand, 100.0 - terrain.GetFeaturePercentage());
                    }
                    break;
                case PlanetType.GiantGas:
                case PlanetType.GiantIce:
                    terrain.AddFeature(TerrainFeature.OtherLand, 100.0);
                    break;
            }

            return terrain;
        }
    }
}
