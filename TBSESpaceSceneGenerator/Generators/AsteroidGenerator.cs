﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBSESpaceSceneGenerator.Structures;

namespace TBSESpaceSceneGenerator.Generators
{
    class AsteroidGenerator
    {
        public AsteroidGenerator() { }

        public Asteroid Generate()
        {
            RandomSingleton random = RandomSingleton.Instance();
            Asteroid asteroid = new Asteroid();

            double typePercentage = random.NextDouble();
            if (typePercentage < 0.75)
                asteroid.Type = AsteroidType.CType;
            else if (typePercentage < 0.92)
                asteroid.Type = AsteroidType.SType;
            else
                asteroid.Type = AsteroidType.MType;

            asteroid.Diameter = random.NextDouble(0.25);
            asteroid.Mass = random.NextDouble() * asteroid.Diameter;
            asteroid.Density = random.NextDouble(1.5, 3.5);
            asteroid.Temperature = random.Next(80, 200);

            return asteroid;
        }

    }
}
